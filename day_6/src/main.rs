use std::collections::HashMap;
use util::{Bounds, Point};

trait FromInputString {
	fn from_str(s: &str) -> Self;
}

impl FromInputString for Point {
	fn from_str(s: &str) -> Self {
		let mut iter = s.split(", ").map(|x| x.parse::<i32>().unwrap()).take(2);
		Point {
			x: iter.next().unwrap(),
			y: iter.next().unwrap(),
		}
	}
}

fn is_point_on_boundary(p: Point, bound: &Bounds) -> bool {
	p.x == bound.top_l.x || p.y == bound.top_l.y || p.x == bound.bot_r.x || p.y == bound.bot_r.y
}

fn get_manhattan_dist(p1: Point, p2: Point) -> usize {
	((p1.x - p2.x).abs() + (p1.y - p2.y).abs()) as usize
}

fn main() -> Result<(), std::io::Error> {
	let x_sorted_in = util::read_text_file("input.txt")?
		.iter()
		.map(|x| Point::from_str(x.as_str()))
		.collect::<Vec<_>>();

	let bounds = util::find_search_space_edges(&x_sorted_in);
	//ownership experiment
	//let mut i = bounds.iter();
	//fn bounds_consumer(_: Bounds) {}
	//bounds_consumer(bounds);

	//part 1
	let mut point_map = HashMap::new();
	let mut dists = Vec::with_capacity(x_sorted_in.len() + 2);
	let mut infinities = Vec::new();
	for p in bounds.iter() {
		dists.extend(
			x_sorted_in
				.iter()
				.map(|inp| (get_manhattan_dist(p, *inp), *inp)),
		);
		//TODO no need to sort the whole thing, we just need the two mins
		//C++ std::partial_sort style
		dists.sort_by(|(d1, _), (d2, _)| d1.cmp(d2));
		if dists[0].0 != dists[1].0 {
			{
				let ent = point_map.entry(dists[0].1).or_insert(0_usize);
				*ent += 1;
			}
			if is_point_on_boundary(p, &bounds) {
				infinities.push(dists[0].1);
			}
		}
		dists.clear();
	}
	for i in infinities {
		point_map.remove(&i);
	}
	println!("{:?}", point_map.values().max().unwrap());

	//part 2
	let mut safe_pts = 0;
	for p in bounds.iter() {
		let sum: usize = x_sorted_in
			.iter()
			.map(|inp| get_manhattan_dist(p, *inp))
			.sum();
		if sum < 10000 {
			safe_pts += 1;
		}
	}
	println!("{}", safe_pts);

	Ok(())
}

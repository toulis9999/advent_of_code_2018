use util::Point;

fn main() -> Result<(), std::io::Error> {
	let velocities = util::read_text_file("input.txt")?
		.iter()
		.map(|x| Point {
			x: x[36..38].trim_start().parse::<i32>().unwrap(),
			y: x[40..42].trim_start().parse::<i32>().unwrap(),
		})
		.collect::<Vec<_>>();

	let mut positions = util::read_text_file("input.txt")?
		.iter()
		.map(|x| Point {
			x: x[10..16].trim_start().parse::<i32>().unwrap(),
			y: x[18..24].trim_start().parse::<i32>().unwrap(),
		})
		.collect::<Vec<_>>();

	let mut num_iters = 0;
	loop {
		num_iters += 1;
		for (pos, vel) in positions.iter_mut().zip(velocities.iter()) {
			pos.x += vel.x;
			pos.y += vel.y;
		}
		let bounds = util::find_search_space_edges(&positions);
		if (bounds.bot_r.y - bounds.top_l.y) == 9 {
			//offset everything to origin
			for pos in positions.iter_mut() {
				pos.x -= bounds.top_l.x;
				pos.y -= bounds.top_l.y;
			}
			break;
		}
	}

	//part 1 - since bounds are small enough I'll solve part 1 the visual way
	let bounds = util::find_search_space_edges(&positions);
	let mut output = vec![vec!['.'; bounds.bot_r.x as usize + 1]; bounds.bot_r.y as usize + 1];
	for p in positions {
		output[p.y as usize][p.x as usize] = '#';
	}
	for q in output {
		for c in q {
			print!("{}", c);
		}
		println!("");
	}

	//part 2
	println!("{}", num_iters);

	Ok(())
}

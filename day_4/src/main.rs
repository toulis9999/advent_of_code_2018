use std::collections::HashMap;
use util;

#[derive(Debug, Copy, Clone)]
enum GuardAction {
	BeginShift(u32), //id nothing else needed
	Sleep(u8),       //min
	Wake(u8),        //min
}

fn parse_schedule_entry(entry: &str) -> GuardAction {
	let nums = entry
		.split(|x: char| !x.is_numeric())
		.filter(|x| *x != "")
		.map(|x| x.parse().unwrap())
		.collect::<Vec<u32>>();
	let words_collection = entry
		.split(|x: char| x.is_numeric())
		.filter(|x| *x != "" && x.len() > 1);

	match words_collection.last().unwrap() {
		" begins shift" => GuardAction::BeginShift(nums[5]),
		//assuming the hours on sleep and wake are always 00
		"] falls asleep" => GuardAction::Sleep(nums[4] as u8),
		"] wakes up" => GuardAction::Wake(nums[4] as u8),
		_ => panic!(),
	}
}

fn produce_guard_data_sheets(actions: &[GuardAction]) -> HashMap<u32, Vec<usize>> {
	//split GuardActions per guard shift
	let mut shift_ranges = Vec::new();
	let mut r_start = 0;
	let mut r_end = 0;
	for (idx, i) in actions.iter().enumerate() {
		match i {
			GuardAction::BeginShift(_) => {
				shift_ranges.push(std::ops::Range {
					start: r_start,
					end: r_end,
				});
				r_start = idx;
				r_end = idx + 1;
			}
			_ => {
				r_end += 1;
			}
		}
	}

	let mut guards = HashMap::new();

	for i in 1..shift_ranges.len() {
		let shift_slice = &actions[shift_ranges[i].clone()];
		//1st Shift action assumed to always be BeginShift
		let guard_id = if let GuardAction::BeginShift(x) = shift_slice[0] {
			x
		} else {
			panic! {}
		};
		let g_e: &mut Vec<usize> = guards.entry(guard_id).or_default();
		if g_e.is_empty() {
			*g_e = vec![0; 60];
		}
		//assuming always pairs of (sleep followed by wake)
		if !shift_slice[1..].is_empty() {
			for sw_pair in shift_slice[1..].chunks(2) {
				let b = if let GuardAction::Sleep(x) = sw_pair[0] {
					x
				} else {
					panic! {}
				} as usize;
				let e = if let GuardAction::Wake(x) = sw_pair[1] {
					x
				} else {
					panic! {}
				} as usize;
				//assume e >= b
				for i in &mut g_e[b..e] {
					*i += 1;
				}
			}
		}
	}
	guards
}

fn get_most_slept_min(totals: &[usize]) -> (usize, usize) {
	totals
		.iter()
		.enumerate()
		.fold((0, 0), |(max_idx, max_val), (idx, v)| {
			if *v > max_val {
				(idx, *v)
			} else {
				(max_idx, max_val)
			}
		})
}

fn main() -> Result<(), std::io::Error> {
	let input = {
		let mut ret = util::read_text_file("input.txt")?;
		//lexicographical sort is enough to give us the right order
		ret.sort();
		ret
	};

	let parsed = input
		.iter()
		.map(|x| parse_schedule_entry(x))
		.collect::<Vec<_>>();

	let guards = produce_guard_data_sheets(&parsed);
	//part 1------------------------------------------------------------------
	let most_sleepy_id = {
		//assume non zero keys
		let mut highest_id = 0;
		let mut highest_mins = 0;
		for (k, v) in &guards {
			let minutes = v.iter().sum();
			if minutes > highest_mins {
				highest_mins = minutes;
				highest_id = *k;
			}
		}
		highest_id
	};

	let most_slept_minute = get_most_slept_min(&guards[&most_sleepy_id]).0;

	println!("{}", most_slept_minute * most_sleepy_id as usize);

	//part 2------------------------------------------------------------------
	let (mut guard_id_with_most_slept_min, mut most_slept_idx, mut max_amount) = (0, 0, 0);
	for (k, v) in guards {
		let (idx, amount) = get_most_slept_min(&v);
		if amount > max_amount {
			guard_id_with_most_slept_min = k;
			most_slept_idx = idx;
			max_amount = amount;
		}
	}

	println!("{}", guard_id_with_most_slept_min as usize * most_slept_idx);
	Ok(())
}

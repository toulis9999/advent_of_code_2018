use util;

fn visit_node<'a, T: Iterator<Item = &'a usize>>(it: &mut T) -> usize {
	let num_child = *it.next().unwrap();
	let num_meta = *it.next().unwrap();
	(0..num_child).map(|_| visit_node(it)).sum::<usize>() + it.take(num_meta).sum::<usize>()
}

fn visit_node_2<'a, T: Iterator<Item = &'a usize>>(it: &mut T) -> usize {
	let num_child = *it.next().unwrap();
	let num_meta = *it.next().unwrap();
	if num_child != 0 {
		let tmp = (0..num_child).map(|_| visit_node_2(it)).collect::<Vec<_>>();
		(0..num_meta).fold(0, |acc, _| {
			acc + *tmp.get(*it.next().unwrap() - 1).get_or_insert(&0) //effin off-by-one gotcha in the problem description
		})
	} else {
		it.take(num_meta).sum()
	}
}

fn main() -> Result<(), std::io::Error> {
	let input = util::read_text_file("input.txt")?
		.iter()
		.cloned()
		//single line input
		.nth(0)
		.unwrap()
		.split(' ')
		.map(|x| x.parse().unwrap())
		.collect::<Vec<usize>>();

	//part 1
	let meta_sum = visit_node(&mut input.iter());
	println!("{}", meta_sum);

	//part 2
	let meta_sum = visit_node_2(&mut input.iter());
	println!("{}", meta_sum);

	Ok(())
}

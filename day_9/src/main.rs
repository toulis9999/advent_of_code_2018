use std::collections::VecDeque;
use util;

#[derive(Debug)]
struct PlayCircle {
	player_scores: Vec<u32>,
	//Vec is way too slow on rotate
	state: VecDeque<u32>,
	current_player_index: usize,
}

impl PlayCircle {
	fn new(num_players: usize, capacity_hint: usize) -> Self {
		assert!(num_players > 1);
		let mut s = VecDeque::with_capacity(capacity_hint + 1);
		s.push_back(0_u32);
		PlayCircle {
			player_scores: vec![0_u32; num_players],
			state: s,
			current_player_index: 0,
		}
	}

	fn place_next_marble(&mut self, marble_pts: u32) {
		if marble_pts % 23 == 0 {
			for _ in 0..7 {
				let elem = self.state.pop_back().unwrap();
				self.state.push_front(elem);
			}
			self.player_scores[self.current_player_index] += marble_pts + self.state.pop_back().unwrap();
			let elem = self.state.pop_front().unwrap();
			self.state.push_back(elem);
		} else {
			let elem = self.state.pop_front().unwrap();
			self.state.push_back(elem);
			self.state.push_back(marble_pts);
		}
		//--------------------------------------------------------------------------------
		self.current_player_index += 1;
		self.current_player_index %= self.player_scores.len();
	}
}

fn main() -> Result<(), std::io::Error> {
	let (num_players, last_marble) = {
		let inp = util::read_text_file("input.txt")?;
		let tmp = inp[0].split(' ').collect::<Vec<_>>();
		(
			tmp[0].parse::<usize>().unwrap(),
			tmp[6].parse::<usize>().unwrap(),
		)
	};

	let mut play_circle = PlayCircle::new(num_players, last_marble * 100);
	for marble in 1..=last_marble * 100 {
		play_circle.place_next_marble(marble as u32);
	}
	println!("{:?}", play_circle.player_scores.iter().max());
	Ok(())
}
